#!/usr/bin/env python3
import xml.etree.ElementTree as ET


class Map(object):
    'this class is used to get information from the xml file'

    #variables
    s_MapXmlPath = ""


    def __init__(self, Spath):
        self.s_MapXmlPath = Spath

        self.xmldata = ET.parse(self.s_MapXmlPath).getroot()
        return

    #@returns(ET.Element)
    def getwaypoint(self, Iid):
        for wp in self.xmldata.findall('waypoint'):
            if (wp.get("id") == str(Iid)):
                return wp
        return -1

    def GetWaypointByColor(self, color1, color2, color3):

        for waypoint in self.xmldata.findall("waypoint"):
            if(waypoint.find('color[@name="color1"]').text == color1 
            and waypoint.find('color[@name="color2"]').text == color2 
            and waypoint.find('color[@name="color3"]').text == color3):
                #print("waypoint nr: " + waypoint.get("id"))
                for direction in waypoint.findall("connection"):
                    #print("direction id:" + direction.get("id") + " side: " + direction.get("direction"))
                    pass
                return waypoint.get("id")
            pass

        return -1

    #@returns(int)
    def GetConnectedWaypoint(self, waypointid, side):
        'returns waypoint id on connected side side(up,down,left,right)'
        wp = self.getwaypoint(waypointid)
        for connection in wp.findall("connection"):
            if(connection.get("direction") == side):
                return int(connection.get("id"))
        return -1

    def IntToDirection(self, directionid):
        if(directionid == 0):
            return "up"
        if(directionid == 1):
            return "right"
        if(directionid == 2):
            return "down"
        if(directionid == 3):
            return "left"
        raise Exception("out of range exeption: directionid must be a (int) between 0-3")

    def DirectionToInt(self, direction):
        if(direction == "up"):
            return 0
        if(direction == "right"):
            return 1
        if(direction == "down"):
            return 2
        if(direction == "left"):
            return 3
        raise Exception("out of range exeption: direction must be a valid direction -> string")

    def GetNextWaypoint(self, currentwp, targetwp, iteration = 0, checked_wp = []):
        'returns (int)waypoint that is closest to target waypoint'
        if(iteration > 100):
            raise OverflowError("recursion to high")

        if(iteration == 0):
            checked_wp.clear()
            checked_wp.append(targetwp)

        optionslist = []
        ShortestRoute = [-1, 999]
        for i in range(0, 4):
            nextwp = self.GetConnectedWaypoint(targetwp, self.IntToDirection(i))

            if(nextwp != -1 and nextwp not in checked_wp):
                if(nextwp == currentwp):
                    return_value = [targetwp, iteration]
                    optionslist.append(return_value)
                    continue
                checked_wp.append(nextwp)
                return_value = self.GetNextWaypoint(currentwp, nextwp, iteration + 1, checked_wp)
                if(return_value != -1):
                        optionslist.append(return_value)
                        continue
        for item in optionslist:
            if(item[1] < ShortestRoute[1]):
                ShortestRoute = item
        if(iteration == 0):
            return ShortestRoute[0]
        return ShortestRoute

    def GetDirectionToWaypoint(self, currentwp, targetwp, direction):
        'Returns direction the robot should turn in, relative to the direction the robot is facing'

        wp = self.getwaypoint(currentwp)
        for item in wp.findall("connection"):
            if(item.get("id") == str(targetwp)):
                targetdirection = self.DirectionToInt(item.get("direction"))
                currentdirectin = direction

                turndirection = self.GetDifferenceInDirection(currentdirectin, targetdirection)

                return turndirection
        raise Exception("Waypoint not found")

    def GetDifferenceInDirection(self, currentdirection, targetdirection):
        'returns relative direction to get from currentdiraction to targetdirection'

        for i in range(0, 4):

            combined = currentdirection + i
            if(combined > 3):
                combined -= 4

            if(combined == targetdirection):
                return i


    def GoTo(self, current_wp, target_wp, facing):
        'takes 3 params: current_wp(int) = the current waypoint, target_wp(int) the target waypoint, facing(int)[between 0-3]'
        if(self.getwaypoint(current_wp) == -1 or self.getwaypoint(target_wp) == -1):
            raise Exception('Map:GOTO - one or more waypoints do not exist')
        if(facing < 0 or facing > 3):
            raise Exception("Map:GOTO - facing must be a int between 0 and 3")

        nextwp = self.GetNextWaypoint(current_wp, target_wp)
        return self.GetDirectionToWaypoint(current_wp, nextwp, facing)
        
